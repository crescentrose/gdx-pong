package co.hpup.pong.World.Objects;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

public class Ball {
    protected Vector2 position;
    protected float speed = 300f;
    protected int width, height;
    protected float angle;
    private Circle hitbox;

    protected static final int BALL_WIDTH = 16;
    protected static final int BALL_HEIGHT = 16;

    public Ball(float x, float y, float angle) {
        this.angle = angle;
        this.position = new Vector2(x, y);
        this.width = BALL_WIDTH;
        this.height = BALL_HEIGHT;
        hitbox = new Circle(position, BALL_WIDTH/2);
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public float getAngle() {
        return angle;
    }

    public float getSpeed() {
        return speed;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void move(float delta) {
        this.position.add(
                (float) Math.sin(Math.toRadians(angle)) * getSpeed() * delta,
                (float) Math.cos(Math.toRadians(angle)) * getSpeed() * delta
        ); // trigonometry, bitches!
    }

    public void bounce(float surfaceAngle, float delta) {
        angle = getBounceAngle(angle, surfaceAngle);
    }

    private float getBounceAngle(float originalAngle, float surfaceAngle) {
        return normaliseAngle((2 * surfaceAngle - originalAngle - 180));
    }

    private float normaliseAngle(float angle) {
        if (angle < 0) {
            return normaliseAngle(angle + 360);
        } else if (angle >= 360) {
            return normaliseAngle(angle - 360);
        }
        return angle;
    }

    public Circle getHitbox() {
        hitbox.setPosition(position.x + BALL_WIDTH / 2, position.y + BALL_HEIGHT / 2);
        return hitbox;
    }
}
