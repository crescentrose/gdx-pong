package co.hpup.pong.World.Objects;

import co.hpup.pong.Input.Action;
import co.hpup.pong.World.GameWorld;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import org.w3c.dom.css.Rect;

public class Player {
    protected Vector2 position;
    protected float speed = 5f;
    protected int width, height;

    protected static final int PADDLE_WIDTH = 16;
    protected static final int PADDLE_HEIGHT = 48;

    private Rectangle hitbox;

    private int score;

    public Player(float x, float y) {
        this.position = new Vector2(x, y);
        this.width = PADDLE_WIDTH;
        this.height = PADDLE_HEIGHT;
        this.hitbox = new Rectangle(x, y, width, height);
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getSpeed() {
        return speed;
    }

    public void moveDown() {
        if (canGoDown()) {
            this.position.add(0, getSpeed() * -1);
        }
    }

    public void moveUp() {
        if (canGoUp()) {
            this.position.add(0, getSpeed());
        }
    }

    public void performAction(Action action) {
        switch (action) {
            case MOVE_DOWN:
                moveDown();
                break;
            case MOVE_UP:
                moveUp();
                break;
        }
    }

    public Rectangle getHitbox() {
        hitbox.setPosition(position);
        return hitbox;
    }

    private boolean canGoUp() {
        return this.getY() + getSpeed() < (GameWorld.WORLD_HEIGHT - this.getHeight());
    }

    private boolean canGoDown() {
        return this.getY() + getSpeed() > 0;
    }

    public void score() {
        score += 1;
    }

    public int getScore() {
        return score;
    }
}
