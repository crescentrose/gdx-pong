package co.hpup.pong.World;

import co.hpup.pong.AI.PongAI;
import co.hpup.pong.World.Objects.Ball;
import co.hpup.pong.World.Objects.Player;

public class GameWorld {

    private Ball ball;
    private Player humanPlayer, computerPlayer;
    private PongAI ai;

    private Simulator simulator;

    public static int WORLD_HEIGHT = 480;
    public static int WORLD_WIDTH = 480;

    public GameWorld() {
        humanPlayer = new Player(10, (WORLD_HEIGHT / 2));
        computerPlayer = new Player(456, WORLD_HEIGHT / 2);
        ball = constructBall();
        ai = new PongAI();
        simulator = new Simulator(this);
    }

    public void update(float delta) {
        // step AI
        computerPlayer.performAction(ai.recommendAction(computerPlayer, ball));
        // step simulation
        simulator.simulate(delta);
    }

    public Player getHumanPlayer() {
        return humanPlayer;
    }

    public Player getComputerPlayer() {
        return computerPlayer;
    }

    public Ball getBall() {
        return ball;
    }

    public void score(Player player) {
        this.ball = constructBall();
        player.score();
    }

    private Ball constructBall()
    {
        return new Ball(WORLD_HEIGHT / 2, WORLD_WIDTH / 2, getRandomAngle());
    }

    private float getRandomAngle() {
        return (float) Math.random() * 60 - 1;
    }
}
