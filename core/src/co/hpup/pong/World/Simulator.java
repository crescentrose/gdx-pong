package co.hpup.pong.World;

import com.badlogic.gdx.math.Intersector;

public class Simulator {
    private GameWorld world;

    public Simulator(GameWorld world) {
        this.world = world;
    }

    public void simulate(float delta) {
        world.getBall().move(delta);

        if (world.getBall().getY() < 0) {
            world.getBall().bounce(0f, delta);
        }
        if (world.getBall().getY() + world.getBall().getHeight() > GameWorld.WORLD_HEIGHT) {
            world.getBall().bounce(180f, delta);
        }

        if (world.getBall().getX() < 0) {
            world.score(world.getHumanPlayer());
        }

        if (world.getBall().getX() + world.getBall().getWidth() > GameWorld.WORLD_WIDTH) {
            world.score(world.getComputerPlayer());
        }

        if (overlapsPlayer()) {
            world.getBall().bounce(90f, delta);
            do {
                world.getBall().move(delta);
            } while (overlapsPlayer());
        }
    }

    private boolean overlapsPlayer() {
        return
            Intersector.overlaps(
                world.getBall().getHitbox(),
                world.getHumanPlayer().getHitbox()
            )
            ||
            Intersector.overlaps(
                world.getBall().getHitbox(),
                world.getComputerPlayer().getHitbox()
            );
    }
}
