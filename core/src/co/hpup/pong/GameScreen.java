package co.hpup.pong;

import co.hpup.pong.Input.ActionDelegator;
import co.hpup.pong.Input.InputHandler;
import co.hpup.pong.Renderer.GameRenderer;
import co.hpup.pong.World.GameWorld;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class GameScreen implements Screen {

    private ActionDelegator actionDelegator;
    private Pong game;
    private GameRenderer renderer;
    private GameWorld world;
    private FitViewport viewport;

    public static final int SCREEN_WIDTH = 480;
    public static final int SCREEN_HEIGHT = 480;

    public GameScreen(Pong game) {
        this.game = game;
        this.viewport = new FitViewport(SCREEN_WIDTH, SCREEN_HEIGHT);
        this.world = new GameWorld();
        this.renderer = new GameRenderer(this.world, this.viewport);
        this.actionDelegator = new ActionDelegator(world.getHumanPlayer());
        Gdx.input.setInputProcessor(new InputHandler(actionDelegator, game.keymap));
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        this.actionDelegator.act();
        this.world.update(delta);
        this.renderer.render();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
