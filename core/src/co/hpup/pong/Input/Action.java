package co.hpup.pong.Input;

public enum Action {
    STAY (false),
    MOVE_UP (true),
    MOVE_DOWN (true),
    MOVE_AI (false);

    private final boolean continuous;

    Action(boolean continuous) {
        this.continuous = continuous;
    }

    public boolean isContinuous() {
        return continuous;
    }
}
