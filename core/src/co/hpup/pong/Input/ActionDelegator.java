package co.hpup.pong.Input;

import co.hpup.pong.World.Objects.Player;
import com.badlogic.gdx.utils.Array;

public class ActionDelegator {

    private Player humanPlayer;

    private Array<Action> currentActions;

    public ActionDelegator(Player humanPlayer) {
        this.humanPlayer = humanPlayer;
        this.currentActions = new Array<Action>();
    }

    public void begin(Action action) {
        currentActions.add(action);
    }

    public void act() {
        for (Action action: currentActions) {
            humanPlayer.performAction(action);
            if ( ! action.isContinuous()) {
                currentActions.removeValue(action, true);
            }
        }
    }

    public void cancel(Action action) {
        currentActions.removeValue(action, true);
    }
}
