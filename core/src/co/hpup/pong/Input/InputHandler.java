package co.hpup.pong.Input;

import com.badlogic.gdx.InputProcessor;

public class InputHandler implements InputProcessor {

    private ActionDelegator delegator;
    private HardcodedKeymap keymap;

    public InputHandler(ActionDelegator delegator, HardcodedKeymap keymap)
    {
        this.delegator = delegator;
        this.keymap = keymap;
    }

    @Override
    public boolean keyDown(int keycode) {
        Action action = keymap.getActionForKey(keycode);
        if (null == action) {
            return false;
        }
        delegator.begin(action);
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        Action action = keymap.getActionForKey(keycode);
        if (null == action) {
            return false;
        }
        delegator.cancel(action);
        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
