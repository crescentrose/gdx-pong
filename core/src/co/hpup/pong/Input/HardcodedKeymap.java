package co.hpup.pong.Input;

import com.badlogic.gdx.Input;

import java.util.HashMap;

public class HardcodedKeymap {
    private HashMap<Integer, Action> keymap;

    public HardcodedKeymap() {
        keymap = new HashMap<Integer, Action>();
        keymap.put(Input.Keys.UP, Action.MOVE_UP);
        keymap.put(Input.Keys.DOWN, Action.MOVE_DOWN);
    }

    public Action getActionForKey(int keycode) {
        return this.keymap.get(keycode);
    }

}
