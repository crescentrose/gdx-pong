package co.hpup.pong;

import co.hpup.pong.Assets.AssetLoader;
import co.hpup.pong.Input.HardcodedKeymap;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Pong extends Game {

    public SpriteBatch batch;
    public BitmapFont font;
    public HardcodedKeymap keymap;

    @Override
    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        keymap = new HardcodedKeymap();

        AssetLoader.load();
        this.setScreen(new GameScreen(this));
    }

    public void render() {
        super.render();
    }

    public void dispose() {
        super.dispose();
        batch.dispose();
        font.dispose();
        AssetLoader.dispose();
    }
}
