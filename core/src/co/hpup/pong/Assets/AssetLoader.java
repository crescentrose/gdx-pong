package co.hpup.pong.Assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class AssetLoader {
    public static Texture paddle, ball;

    public static void load() {
        paddle = new Texture(Gdx.files.internal("pad.png"));
        paddle.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);

        ball = new Texture(Gdx.files.internal("ball.png"));
        ball.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
    }

    public static void dispose() {
        paddle.dispose();
        ball.dispose();
    }
}
