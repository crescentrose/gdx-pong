package co.hpup.pong.Renderer;

import co.hpup.pong.Assets.AssetLoader;
import co.hpup.pong.GameScreen;
import co.hpup.pong.World.GameWorld;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.Viewport;

public class GameRenderer {

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private GameWorld world;
    private BitmapFont font;

    private ShapeRenderer debug;

    private static final boolean isDebugOn = false;

    public GameRenderer(GameWorld world, Viewport viewport) {
        this.world = world;

        camera = new OrthographicCamera();
        // we are not using yDown, meaning that we have a true Cartesian system in which (0, 0) is the lower left corner
        // like God intended
        camera.setToOrtho(false, GameScreen.SCREEN_WIDTH, GameScreen.SCREEN_HEIGHT);

        viewport.setCamera(camera);

        font = new BitmapFont();

        batch = new SpriteBatch();
        batch.setProjectionMatrix(camera.combined);

        debug = new ShapeRenderer();
        debug.setProjectionMatrix(camera.combined);
    }

    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(
                AssetLoader.paddle,
                world.getHumanPlayer().getX(),
                world.getHumanPlayer().getY(),
                world.getHumanPlayer().getWidth(),
                world.getHumanPlayer().getHeight()
        );
        batch.draw(
                AssetLoader.paddle,
                world.getComputerPlayer().getX(),
                world.getComputerPlayer().getY(),
                world.getComputerPlayer().getWidth(),
                world.getComputerPlayer().getHeight()
        );
        batch.draw(
                AssetLoader.ball,
                world.getBall().getX(),
                world.getBall().getY(),
                world.getBall().getWidth(),
                world.getBall().getHeight()
        );

        // draw score
        font.draw(batch, String.valueOf(world.getComputerPlayer().getScore()), GameScreen.SCREEN_WIDTH / 3, 50);
        font.draw(batch, String.valueOf(world.getHumanPlayer().getScore()), GameScreen.SCREEN_WIDTH / 3 * 2, 50);

        batch.end();


        if (!isDebugOn) {
            return;
        }

        debug.setColor(Color.RED);
        debug.setAutoShapeType(true);
        debug.begin();
        debug.rect(
                world.getHumanPlayer().getHitbox().x,
                world.getHumanPlayer().getHitbox().y,
                world.getHumanPlayer().getHitbox().width,
                world.getHumanPlayer().getHitbox().height
        );
        debug.rect(
                world.getComputerPlayer().getHitbox().x,
                world.getComputerPlayer().getHitbox().y,
                world.getComputerPlayer().getHitbox().width,
                world.getComputerPlayer().getHitbox().height
        );
        debug.circle(
                world.getBall().getHitbox().x,
                world.getBall().getHitbox().y,
                world.getBall().getHitbox().radius
        );
        debug.end();
    }
}
