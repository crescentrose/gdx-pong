package co.hpup.pong.AI;

import co.hpup.pong.Input.Action;
import co.hpup.pong.World.GameWorld;
import co.hpup.pong.World.Objects.Ball;
import co.hpup.pong.World.Objects.Player;

public class PongAI {
    public Action recommendAction(Player player, Ball ball) {

        if (ball.getX() < GameWorld.WORLD_HEIGHT / 2) {
            return Action.STAY;
        }

        double likelyImpactPoint = Math.tan(ball.getAngle()) * (player.getX() - ball.getX()) + ball.getY();

        if (likelyImpactPoint > player.getY() + player.getHeight() - 10) {
            // if ball is above paddle, go up
            return Action.MOVE_UP;
        } else if (likelyImpactPoint < player.getY() + 20) {
            // else go down
            return Action.MOVE_DOWN;
        }

        return Action.STAY;
    }
}
