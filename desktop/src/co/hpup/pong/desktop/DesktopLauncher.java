package co.hpup.pong.desktop;

import co.hpup.pong.GameScreen;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import co.hpup.pong.Pong;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = GameScreen.SCREEN_WIDTH;
		config.height = GameScreen.SCREEN_HEIGHT;
        config.title = "GDXPong";

		new LwjglApplication(new Pong(), config);
	}
}
